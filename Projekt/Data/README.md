# Daten

http://www.eecs.qmul.ac.uk/~hs308/WebLogo-2M.html/

## Daten entpacken
tar -xvzf NAMEDERDATEI.tar.gz

## Daten zufällig verschieben/kopieren
10: Anzahl wie viele Daten zufällig verschoben werden sollen

verschieben

ls | shuf -n 10 | xargs -i mv {} path-to-new-folder

kopieren

ls | shuf -n 10 | xargs -i cp {} path-to-new-folder