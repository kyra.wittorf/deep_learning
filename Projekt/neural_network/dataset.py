import matplotlib.pyplot as plt
import numpy as np

import torchvision
import torch
import torch.utils.data
from torchvision.transforms import transforms

from definitions import ROOT_DIR
from neural_network.params import Params
from neural_network.subset import MySubset

train_data_dir = ROOT_DIR + "/Data/" + Params.train_data_dir + "/train"


def load_train_dataset():
    """
    load the train and the validation dataset

    :return: the train data loader and the validation data loader
    """
    train_transformations = transforms.Compose([
        transforms.Resize((Params.image_size, Params.image_size)),
        transforms.ToTensor(),
        transforms.Normalize(Params.image_mean, Params.image_std),
    ])
    t_dataset, v_dataset = split_dataset(train_transformations, Params.percentage_validation_dataset)

    train_data_loader = torch.utils.data.DataLoader(
        t_dataset,
        batch_size=Params.batch_size_train,
        shuffle=True,
    )

    validation_data_loader = torch.utils.data.DataLoader(
        v_dataset,
        batch_size=Params.batch_size_train,
        shuffle=True,
    )

    if Params.show_images:
        show_images(validation_data_loader, v_dataset)
        show_images(train_data_loader, t_dataset)

    return train_data_loader, validation_data_loader


def split_dataset(train_transformations, validation_percentage: float = 0.1):
    """
    split the whole dataset into two subsets

    the first subset is the train_dataset, which have 1-validation_percentage length of the whole dataset
    the second subset is the validation_dataset with size: validation_percentage * len(train_validation_dataset)

    :param train_transformations: transformations of the images
    :param validation_percentage: percentage of the validation dataset
    :return: train_dataset, validation_dataset
    """
    train_validation_dataset = torchvision.datasets.ImageFolder(
        train_data_dir,
        transform=train_transformations
    )

    print("Length whole dataset: ", len(train_validation_dataset))

    amount_all_data = len(train_validation_dataset)
    amount_validation_data = int(amount_all_data * validation_percentage)
    amount_train_data = amount_all_data - amount_validation_data

    t_dataset, v_dataset = torch.utils.data.random_split(train_validation_dataset,
                                                         [amount_train_data, amount_validation_data])

    v_labels = list(map(lambda x: train_validation_dataset.targets[x], v_dataset.indices))
    t_labels = list(map(lambda x: train_validation_dataset.targets[x], t_dataset.indices))

    v_dataset = MySubset(train_validation_dataset, v_dataset.indices, v_labels)
    t_dataset = MySubset(train_validation_dataset, t_dataset.indices, t_labels)

    print("Train length", len(t_dataset))
    print("Train Dataset: \n ", t_dataset)
    print("Train Labels: \n", t_dataset.targets)

    print("Validation length", len(v_dataset))
    print("Validation Dataset: \n ", v_dataset)
    print("Validation Labels: \n", v_dataset.targets)

    return t_dataset, v_dataset


def load_test_dataset():
    """
    load the test dataset from the test folder

    :return: test data loader
    """
    train_transformations = transforms.Compose([
        transforms.Resize((Params.image_size, Params.image_size)),
        transforms.ToTensor(),
        transforms.Normalize(Params.image_mean, Params.image_std),
    ])

    dataset = torchvision.datasets.ImageFolder(
        ROOT_DIR + "/Data/" + Params.test_data_dir + "/test",
        transform=train_transformations
    )

    print("Test Dataset: \n ", dataset)
    print("Test Labels: \n", dataset.targets)

    data_loader = torch.utils.data.DataLoader(
        dataset,
        batch_size=Params.batch_size_test,
        shuffle=True,
    )

    if Params.show_images:
        show_images(data_loader, dataset)

    return data_loader


def show_images(data_loader, data_set):
    """
    shows for images of the given data_loader

    :param data_loader: the data loader
    :param data_set: the data set to the given data loader
    :return: nothing, shows images and printed their labels on the console
    """
    images, labels = next(iter(data_loader))

    classes = data_set.classes
    print("Classes of the presented images:", ', '.join(classes[i] for i in labels[:4]))
    img = torchvision.utils.make_grid(images[:4])
    np_img = img.numpy().transpose((1, 2, 0))
    np_img = np_img * Params.image_std + Params.image_mean
    np_img = np.clip(np_img, 0, 1)
    plt.imshow(np_img)
    plt.show()
