from neural_network.logo_network.ConvNet3 import ConvNet3

import numpy as np
import torch


class Params:
    batch_size_test = 32
    batch_size_train = 128
    # image_size = 224
    image_size = 128
    image_mean = np.array([0.44943, 0.4331, 0.40244])
    image_std = np.array([0.29053, 0.28417, 0.30194])

    log_interval = 50  # Number of iterations before mini-batch statistics are logged
    nr_epochs = 50  # Number of epochs during training
    save_model_epochs = [30]

    loss_function = torch.nn.CrossEntropyLoss()

    # Adam Optimizer
    optimizer = "Adam"
    learning_rate = 0.0001
    weight_decay = 0.0
    lr_update_interval = 10  # in epochs
    lr_update_factor = 0.5

    # SGD Optimizer
    # optimizer = "SGD"
    sgd_learning_rate = 0.001
    sgd_momentum = 0.9

    # Choose a network
    # network = ConvNet
    # network = ConvNet2
    network = ConvNet3
    # network = ConvNet4
    # network = ConvNet5
    # network = ConvNet6
    # network = ConvNet7

    # train_data_dir = "mini-dataset"
    # test_data_dir = "mini-dataset"
    # percentage_validation_dataset = 0.5

    train_data_dir = "big-dataset-juli-19"
    test_data_dir = "big-dataset-juli-19"
    percentage_validation_dataset = 0.1

    classes = ['amazon', 'ebay', 'microsoft']

    show_images = True

    # This setting is only for Max (or hole data runs on the big dataset, which we can push on git)
    important_models = False

    our_final_model_folder = "ConvNet3-50-epochs-model-2020-07-19_20-59-41/"
