import matplotlib.pyplot as plt


def plot_kernels(tensor, num_cols=6):
    if not tensor.ndim == 4:
        raise Exception("Assumes a 4D tensor")
    if not tensor.shape[-1] == 3:
        raise Exception("Last dim needs to be 3 to plot")

    num_kernels = tensor.shape[0]
    num_rows = num_kernels + 1
    fig = plt.figure(figsize=(num_cols, num_rows))

    for i in range(num_kernels):
        ax = fig.add_subplot(num_rows, num_cols, i + 1)
        ax.img_show(tensor[i])
        ax.axis('off')
        ax.set_xticklabels([])
        ax.set_yticklabels([])

    plt.subplots_adjust(wspace=0.1, hspace=0.1)
    plt.show()
