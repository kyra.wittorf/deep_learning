import os
import shutil
import torchvision

from definitions import ROOT_DIR
from neural_network.params import Params

train_data_dir = ROOT_DIR + "/Data/" + Params.train_data_dir + "/train"


def split_dataset(batch_size, train_transformations):
    merge_train_and_validation_dirs()
    split_train_and_validation_dirs(batch_size)

    train_dataset = torchvision.datasets.ImageFolder(
        train_data_dir + "/train_subset",
        transform=train_transformations
    )

    validation_dataset = torchvision.datasets.ImageFolder(
        train_data_dir + "/validation_subset",
        transform=train_transformations
    )
    return train_dataset, validation_dataset


# Always splits the first batch_size elements of each class directory into the validation_subset directory and the rest
#  into the train_subset directory
def split_train_and_validation_dirs(batch_size):
    class_dirs_list = os.listdir(train_data_dir)

    os.makedirs(train_data_dir + "/train_subset")
    os.makedirs(train_data_dir + "/validation_subset")
    for class_dir in class_dirs_list:
        class_elements = os.listdir(train_data_dir + "/" + class_dir)
        os.makedirs(train_data_dir + "/validation_subset/" + class_dir)
        for i in range(batch_size):
            filename = train_data_dir + "/" + class_dir + "/" + class_elements[i]
            shutil.move(filename, train_data_dir + "/validation_subset/" + class_dir)
            class_elements.remove(class_elements[i])

        os.makedirs(train_data_dir + "/train_subset/" + class_dir)
        for filename in class_elements:
            shutil.move(train_data_dir + "/" + class_dir + "/" + filename,
                        train_data_dir + "/train_subset/" + class_dir)

        os.rmdir(train_data_dir + "/" + class_dir)


def merge_train_and_validation_dirs():
    if os.listdir(train_data_dir).__contains__('train_subset'):
        for dir in os.listdir(train_data_dir + "/train_subset"):
            shutil.move(train_data_dir + "/train_subset/" + dir, train_data_dir)
        os.rmdir(train_data_dir + "/train_subset")

    if os.listdir(train_data_dir).__contains__('validation_subset'):
        for dir in os.listdir(train_data_dir + "/validation_subset"):
            for file in os.listdir(train_data_dir + "/validation_subset/" + dir):
                shutil.move(train_data_dir + "/validation_subset/" + dir + "/" + file, train_data_dir + "/" + dir)
        shutil.rmtree(train_data_dir + "/validation_subset")
