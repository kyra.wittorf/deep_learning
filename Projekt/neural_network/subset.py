from torch.utils.data import Dataset
import torch
from torchvision.datasets import ImageFolder

from neural_network.params import Params


class MySubset(Dataset):
    """
    Subset of a dataset at specified indices.

    Arguments:
        dataset (Dataset): The whole Dataset
        indices (sequence): Indices in the whole set selected for subset
        labels(sequence) : targets as required for the indices. will be the same length as indices
    """
    def __init__(self, dataset: ImageFolder, indices: list, labels: list):
        self.dataset = dataset
        self.indices = indices
        self.targets = labels
        self.labels_tensor = torch.ones(len(self.targets)).type(torch.long) * 300

        self.classes = Params.classes
        labels_hold = torch.ones(len(dataset)).type(torch.long) * 300 #( some number not present in the #labels just to make sure
        for i in range(len(indices)):
            labels_hold[self.indices[i]] = labels[i]
        for i in range(len(indices)):
            self.labels_tensor[i] = self.targets[i]
        self.labels = labels_hold

    def __getitem__(self, idx):
        """
        get the idx data item with label
        :param idx: integer value, index of the element
        :return: the image and the corresponding label
        """
        image = self.dataset[self.indices[idx]][0]
        label = self.labels[self.indices[idx]]

        return image, label

    def __len__(self):
        """
        :return: amount of data in the
        """
        return len(self.indices)

    def get_all(self):
        """
        :return: get the full dataset with labels
        """
        dataset = map(lambda x: self.dataset, self.indices)
        labels = self.targets

        return dataset, labels
