import torch
from PIL import Image
from torchvision import transforms
import torch.utils.data

from definitions import ROOT_DIR
from neural_network.params import Params


class Prediction:
    def __init__(self, model_folder_path):
        """
        initalize the prediction for a given model

        :param model_folder_path: model which is to test
        """
        cuda_available = torch.cuda.is_available()
        self.device = torch.device('cpu')
        if cuda_available:
            self.device = torch.device('cuda:0')

        self.model = Params.network(Params.image_size)
        model_path = model_folder_path + 'model.pth'
        self.model.load_state_dict(torch.load(model_path, map_location=self.device))

    def image_predict(self, image_path):
        """
        predict the given image

        :param image_path: path of the image
        :return: nothing, printed the accuracies of the classes
        """
        test_image = self.__load_single_picture(image_path)
        self.model.eval()
        self.model.to(self.device)
        # context-manager that disabled gradient computation
        with torch.no_grad():
            outputs = self.model(test_image)

            # return the maximum value of each row of the input tensor in the
            # given dimension dim, the second return vale is the index location
            # of each maxium value found(argmax)
            _, predicted = torch.max(outputs.data.to(self.device), dim=1)

            results = outputs.cpu().numpy()[0]
            print('Amazon: %.4f \nEbay: %.4f \nMicrosoft: %.4f' % (results[0], results[1], results[2]))

            if predicted == 0:
                print('This is a picture with Amazon logo')
            elif predicted == 1:
                print('This is a picture with Ebay logo')
            elif predicted == 2:
                print('This is a picture with Microsoft logo')

    def __load_single_picture(self, image_path):
        """
        load a single picture from the image_path

        :param image_path: path of the image
        :return: image tensor of the given image
        """
        single_picture = Image.open(image_path)
        transformations = transforms.Compose([
            transforms.Resize((Params.image_size, Params.image_size)),
            transforms.ToTensor(),
            transforms.Normalize(Params.image_mean, Params.image_std),
        ])
        single_picture = transformations(single_picture).float()

        single_picture = single_picture.unsqueeze(0)
        return single_picture.to(self.device)


if __name__ == "__main__":
    pred = Prediction(ROOT_DIR + "/important_models/" + Params.our_final_model_folder)
    pred.image_predict(ROOT_DIR + "image")
