import torch
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

import os
import math
import numpy as np
from datetime import datetime

from definitions import ROOT_DIR
from neural_network.dataset import load_train_dataset
from neural_network.Documentation.plotting import Plotting

from neural_network.params import Params
from neural_network.test_model import evaluate_model

cuda_available = torch.cuda.is_available()
device = torch.device('cpu')

print("Uses CUDA:", cuda_available)
if cuda_available:
    device = torch.device('cuda:0')
    print("CUDA Device:", torch.cuda.get_device_name(device))


class TrainModel:
    def __init__(self, seed=1):
        self.train_loader, self.validation_loader = load_train_dataset()
        self.train_dataset_len = len(self.train_loader.dataset)

        self.input_size = Params.image_size

        self.loss_function = Params.loss_function

        self.time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        if Params.important_models:
            self.save_path = ROOT_DIR + '/important_models/model-' + self.time
            self.log_path = ROOT_DIR + '/important_models/runs/' + self.time
        else:
            self.save_path = ROOT_DIR + '/Modelle/model-' + self.time
            self.log_path = '../runs/' + self.time

        # os.makedirs(self.save_path)

        torch.manual_seed(seed)

        self.model = Params.network(self.input_size)

    def train_model(self):
        """
        train model and make a history

        :return: nothing (save a model)
        """
        writer = SummaryWriter(self.log_path)

        images, labels = next(iter(self.train_loader))
        writer.add_graph(self.model, images)

        self.model.to(device=device)

        print("Model: \n", self.model)

        if Params.optimizer == "Adam":
            optimizer = optim.Adam(self.model.parameters(), lr=Params.learning_rate, weight_decay=Params.weight_decay)
        elif Params.optimizer == "SGD":
            optimizer = optim.SGD(self.model.parameters(), lr=Params.sgd_learning_rate, momentum=Params.sgd_momentum)
        else:
            Exception("You must define a new optimizer or choose a right one.")

        total_train_duration = 0
        loss_train_values = []
        acc_train_values = []
        loss_val_values = []
        acc_val_values = []

        for epoch in range(Params.nr_epochs):  # loop over the dataset multiple times
            self.model.train()
            print("Epoch: %d/%d" % (epoch + 1, Params.nr_epochs))
            nr_batches = math.ceil(len(self.train_loader.dataset) / float(self.train_loader.batch_size))

            epoch_start_time = datetime.now()
            mini_batch_start_time = epoch_start_time

            epoch_loss = 0.0
            running_loss = 0.0

            epoch_correct_train = 0

            # Adapt learning rate
            if epoch % Params.lr_update_interval == Params.lr_update_interval - 1:
                for param_group in optimizer.param_groups:
                    lr = param_group['lr'] * Params.lr_update_factor
                    param_group['lr'] = lr
                    print("New learning rate: %f" % lr)

            # Save model after certain number of epochs
            if epoch - 1 in Params.save_model_epochs:
                export_path = self.save_path + '-epoch-' + str(epoch - 1)
                os.makedirs(export_path)
                self.save_model(export_path, optimizer)

            for i, data in enumerate(self.train_loader, 0):
                # get the inputs; data is a list of [inputs, labels]
                inputs, labels = data

                # convert inputs to cuda tensors if needed
                if cuda_available:
                    inputs = inputs.to(device)
                    labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward + backward + optimize
                outputs = self.model(inputs)

                loss = self.loss_function.forward(outputs, labels)

                loss.backward()
                optimizer.step()

                epoch_loss += outputs.shape[0] * loss.item()
                running_loss += loss.item()

                epoch_correct_train += self.correct(outputs, labels)

                # print statistics after each mini-batches
                if i % Params.log_interval == Params.log_interval - 1:
                    mini_batch_duration = (datetime.now() - mini_batch_start_time).total_seconds() * 1000
                    mini_batch_start_time = datetime.now()

                    print('[Batch %d/%d] loss: %.3f, duration: %dms' %
                          (i + 1, nr_batches, running_loss / Params.log_interval, mini_batch_duration))
                    running_loss = 0.0

            epoch_duration = (datetime.now() - epoch_start_time).total_seconds() * 1000
            total_train_duration += epoch_duration
            epoch_loss /= self.train_dataset_len
            epoch_correct_train /= self.train_dataset_len

            loss_train_values.append(epoch_loss)
            acc_train_values.append(epoch_correct_train)

            loss_validation, acc_validation = self.evaluate(self.validation_loader)
            loss_val_values.append(loss_validation)
            acc_val_values.append(acc_validation)

            writer.add_scalar('Loss/Train', epoch_loss, epoch)
            writer.add_scalar('Accuracy/Train', epoch_correct_train, epoch)
            writer.add_scalar('Loss/Validation', loss_validation, epoch)
            writer.add_scalar('Accuracy/Validation', acc_validation, epoch)

            print("Epoch Train/Validation | Loss: %.3f/%.3f, Accuracy: %3f/%.3f, duration: %dms" % (
                epoch_loss, loss_validation, epoch_correct_train, acc_validation, epoch_duration))

        writer.close()
        print("Total train duration: %dms" % total_train_duration)

        # Save model and optimizer
        export_path = self.save_path + '-epoch-' + str(Params.nr_epochs)
        os.makedirs(export_path)
        self.save_model(export_path, optimizer)

        evaluate_model(self.model, self.validation_loader)
        Plotting.plot_train_validation_accuracy(export_path, acc_train_values, acc_val_values,
                                                range(1, Params.nr_epochs + 1))
        Plotting.plot_train_validiation_loss(export_path, loss_train_values, loss_val_values,
                                             range(1, Params.nr_epochs + 1))

    def save_model(self, folder_path, optimizer):
        """
        save model in the folder_path

        :param folder_path: directory from the model
        :param optimizer: optimizer
        :return: nothing
        """
        torch.save(self.model.state_dict(), folder_path + '/model.pth')
        torch.save(optimizer.state_dict(), folder_path + '/optimizer.pth')

    def correct(self, outputs, labels):
        """
        count the correct predictions

        :param outputs: probabilities of each class for each image
        :param labels: expected label for each image
        :return: total count of correct classified images
        """
        y_pred = np.argmax(outputs.detach().cpu(), axis=1)
        y = labels.detach()

        correct = 0
        for i in range(len(y)):
            if y[i] == y_pred[i]:
                correct += 1
        return correct

    def evaluate(self, data_loader):
        """
        calculate the loss and the accuracy of the current model

        :param data_loader: on this data loader
        :return: loss, accuracy on the given data
        """
        self.model.eval()
        with torch.no_grad():
            loss = 0.0
            accuracy = 0.0
            batches = math.ceil(len(data_loader.dataset) / data_loader.batch_size)
            for inputs, labels in data_loader:
                labels = labels.to(device)
                outputs = self.model(inputs.to(device))
                loss += self.loss_function(outputs, labels)
                accuracy += self.correct(outputs, labels)
            loss /= batches
            accuracy /= len(data_loader.dataset)
        return loss.cpu().numpy(), accuracy


if __name__ == "__main__":
    train_model = TrainModel()
    train_model.train_model()
