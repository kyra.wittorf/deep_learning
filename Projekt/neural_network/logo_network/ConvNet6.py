import torch.nn as nn
import torch


class ConvNet6(nn.Module):
    def __init__(self, data_size):
        """
        size = Resize Image - for example 32, 256, ...
        """
        super(ConvNet6, self).__init__()
        # input: size x size x 3
        self.layer1 = nn.Sequential(
            #  size x size x 32
            nn.Conv2d(3, 32, kernel_size=11, stride=1, padding=5),
            nn.ReLU(),
            # nn.Conv2d(32, 32, kernel_size=11, stride=1, padding=5),
            # nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        # size/2 x size/2 x 32
        self.layer2 = nn.Sequential(
            # size/2 x size/2 x 64
            nn.Conv2d(32, 64, kernel_size=7, stride=1, padding=3),
            nn.ReLU(),
            # nn.Conv2d(64, 64, kernel_size=7, stride=1, padding=3),
            # nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        # size/4 x size/4 x 64
        self.layer3 = nn.Sequential(
            # size/4 x size/4 x 128
            nn.Conv2d(64, 128, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            # nn.Conv2d(128, 128, kernel_size=5, stride=1, padding=2),
            # nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        # size/8 x size/8 x 128
        self.layer4 = nn.Sequential(
            # size/8 x size/8 x 256
            nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            # nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
            # nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        # size/16 x size/16 x 256
        self.drop_out = nn.Dropout()
        self.fc1 = nn.Linear(16384, 4096)
        self.fc2 = nn.Linear(4096, 512)
        self.fc3 = nn.Linear(512, 3)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = out.reshape(out.size(0), -1)
        out = self.drop_out(out)
        out = self.fc1(out)
        out = self.fc2(out)
        out = torch.softmax(self.fc3(out), dim=1)
        return out
