import torch.nn as nn
import torch


class ConvNet(nn.Module):
    def __init__(self, data_size):
        """
        size = Resize Image - for example 32, 256, ...
        """
        super(ConvNet, self).__init__()
        # input: size x size x 3
        self.layer1 = nn.Sequential(
            #  size x size x 32
            nn.Conv2d(3, 32, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        # size/2 x size/2 x 32
        self.layer2 = nn.Sequential(
            # size/2 x size/2 x 64
            nn.Conv2d(32, 64, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        # size/4 x size/4 x 64
        self.drop_out = nn.Dropout()
        self.fc1 = nn.Linear(int(data_size/4) * int(data_size/4) * 64, 1000)
        self.fc2 = nn.Linear(1000, 3)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.reshape(out.size(0), -1)
        out = self.drop_out(out)
        out = self.fc1(out)
        out = torch.softmax(self.fc2(out), dim=1)
        return out
