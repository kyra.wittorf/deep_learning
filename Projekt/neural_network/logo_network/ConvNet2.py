import torch.nn as nn
import torch


class ConvNet2(nn.Module):
    def __init__(self, data_size):
        self.data_size = data_size

        # input: size x size x 3
        super(ConvNet2, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        # after conv1: size-4 x size-4 x 6
        self.pool = nn.MaxPool2d((2, 2))
        # after: size/2 - 2 x size/2 - 2 x 6
        self.conv2 = nn.Conv2d(6, 16, 5)
        # after: size/2 - 6 x size/2 - 6 x 16
        self.img_size = int((self.data_size / 2 - 6) / 2)
        self.fc1 = nn.Linear(16 * self.img_size * self.img_size, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 3)

    def forward(self, x):
        x = self.pool(torch.relu(self.conv1(x)))
        x = self.pool(torch.relu(self.conv2(x)))
        x = x.view(-1, 16 * self.img_size * self.img_size)
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = self.fc3(x)
        return x
