import torch.nn as nn
import torch


class ConvNet5(nn.Module):
    def __init__(self, data_size):
        """
        size = Resize Image - for example 32, 256, ...
        """
        super(ConvNet5, self).__init__()
        # input: size x size x 3
        self.layer1 = nn.Sequential(
            #  size x size x 64
            nn.Conv2d(3, 64, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
        )
        # size/2 x size/2 x 32
        self.layer2 = nn.Sequential(
            # size/2 x size/2 x 64
            nn.Conv2d(64, 128, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
        )
        # size/4 x size/4 x 128
        self.layer3 = nn.Sequential(
            # size/4 x size/4 x 32
            nn.Conv2d(128, 256, kernel_size=7, stride=1, padding=3),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
        )
        # size/8 x size/8 x 256
        self.layer4 = nn.Sequential(
            # size/8 x size/8 x 512
            nn.Conv2d(256, 512, kernel_size=7, stride=1, padding=3),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        # size/16 x size/16 x 512
        self.drop_out = nn.Dropout()
        self.fc1 = nn.Linear(int(data_size/16) * int(data_size/16) * 512, 1024)
        self.fc2 = nn.Linear(1024, 3)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = out.reshape(out.size(0), -1)
        out = self.drop_out(out)
        out = self.fc1(out)
        out = torch.softmax(self.fc2(out), dim=1)
        return out
