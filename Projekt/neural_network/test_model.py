import torch
import numpy as np

from sklearn.metrics import classification_report, confusion_matrix

from definitions import ROOT_DIR
from neural_network.dataset import  load_test_dataset

from neural_network.params import Params

cuda_available = torch.cuda.is_available()
device = torch.device('cpu')

print("Uses CUDA:", cuda_available)
if cuda_available:
    device = torch.device('cuda:0')
    print("CUDA Device:", torch.cuda.get_device_name(device))


def evaluate_model(network, test_loader):
    """
    print the classification report, confusion matrix and accuracy for a given model and a given test set

    :param network: model which is to evaluate
    :param test_loader: test data loader
    :return: nothing
    """
    network.eval()
    expected = []
    predicted = []
    with torch.no_grad():
        for data, target in test_loader:
            expected = np.append(expected, target.numpy())
            output = network(data.to(device))
            pred = torch.max(output.data.to(device), 1)
            predicted = np.append(predicted, pred[1].cpu())
    print("Classification report:")
    print(classification_report(expected, predicted))
    print("Confusion matrix:")
    print(confusion_matrix(expected, predicted))
    accuracy(predicted, expected)


def accuracy(predicted, expected):
    """
    calulate the accuracy from the predicted values

    :param predicted: predicted values
    :param expected: expected values
    :return: the accuracy
    """
    correct = 0
    for i in range(len(predicted)):
        if predicted[i] == expected[i]:
            correct += 1
    acc = correct / len(predicted)
    print("Accuracy:", acc)
    return acc


def test_saved_model(model_folder):
    """
    test a saved model with the test_dataset

    :param model_folder: given model path
    :return: nothing, print the accuracy and the confusion matrix and classification report
    """
    input_data = load_test_dataset()
    model = Params.network(Params.image_size)
    model.to(device)
    model_path = ROOT_DIR + '/Modelle/' + model_folder + 'model.pth'
    model.load_state_dict(torch.load(model_path, map_location=device))
    evaluate_model(model, input_data)


if __name__ == "__main__":
    test_saved_model('../important_models/ConvNet3-50-epochs-model-2020-07-19_20-59-41/')
