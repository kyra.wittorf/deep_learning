import json

from definitions import ROOT_DIR
from neural_network.Documentation.plotting import Plotting


def import_json(file_path):
    """
    import json (from tensorboard)

    :param file_path: path to the json file
    :return: epochs, values (could be accuracy or loss)
    """
    with open(file_path, 'r') as f:
        datastore = json.load(f)

    epochs = []
    values = []
    for i in datastore:
        epochs.append(i[1] + 1)
        values.append(i[2])

    return epochs, values


if __name__ == "__main__":
    model_path = ROOT_DIR + "/important_models/ConvNet3-50-epochs-model-2020-07-19_20-59-41/"
    epochs, acc_train = import_json(model_path + "run-ConvNet3-50-epochs-2020-07-19_20-59-41-tag-Accuracy_Train.json")
    epochs, acc_val = import_json(
        model_path + "run-ConvNet3-50-epochs-2020-07-19_20-59-41-tag-Accuracy_Validation.json")

    Plotting.plot_train_validation_accuracy(model_path, acc_train, acc_val, epochs)

    epochs, loss_train = import_json(model_path + "run-ConvNet3-50-epochs-2020-07-19_20-59-41-tag-Loss_Train.json")
    epochs, loss_val = import_json(model_path + "run-ConvNet3-50-epochs-2020-07-19_20-59-41-tag-Loss_Validation.json")

    Plotting.plot_train_validiation_loss(model_path, loss_train, loss_val, epochs)
