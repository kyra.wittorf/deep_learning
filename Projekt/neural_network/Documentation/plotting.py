import matplotlib.pyplot as plt


class Plotting:
    @staticmethod
    def plot_train_validiation_loss(save_path, train_loss, val_loss, epochs):
        """
        save and show a plot of the trainingsprocess of the loss

        :param save_path: directory to save in
        :param train_loss: train loss values
        :param val_loss: validation loss values (same length as train_loss)
        :param epochs: epochs values (same length as train_loss)
        :return: nothing
        """
        plt.plot(epochs, Plotting.smooth_curve(train_loss), 'g', label='Train')
        plt.plot(epochs, Plotting.smooth_curve(val_loss), 'b', label=' Validation')
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.title("Loss Train/Validation (Smoothed)")
        plt.legend()
        plt.savefig(save_path + "/loss.png")
        plt.show()

    @staticmethod
    def plot_train_validation_accuracy(save_path, train_acc, val_acc, epochs):
        """
        save and show a plot of the trainingsprocess of the accuracy

        :param save_path: directory to save in
        :param train_acc: train accuracy values
        :param val_acc: validation accurancy values (same length as train_acc)
        :param epochs: epochs values (same length as train_acc)
        :return: nothing
        """
        plt.plot(epochs, Plotting.smooth_curve(train_acc), 'g', label='Train')
        plt.plot(epochs, Plotting.smooth_curve(val_acc), 'b', label=' Validation')
        plt.xlabel("Epoch")
        plt.ylabel("Accuracy")
        plt.title("Accuracy Train/Validation (smoothed)")
        plt.legend()
        plt.savefig(save_path + "/accuracy.png")
        plt.show()

    @staticmethod
    def smooth_curve(points, factor=0.8):
        """
        this make the curve of the points (loss or accuracy values) smooth

        :param points: points (loss or accuracy values)
        :param factor: smoothing factor
        :return: the transformed points which are now smoothed points
        """
        smoothed_points = []
        for point in points:
            if smoothed_points:
                previous = smoothed_points[-1]
                smoothed_points.append(previous * factor + point * (1 - factor))
            else:
                smoothed_points.append(point)
        return smoothed_points
