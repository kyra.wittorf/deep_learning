## How to use Tensorboard

1. Install tensorboard using ``pip install tensorboard``.
2. Open a terminal within the `` ../Projekt`` folder. 
3. Run `` tensorboard --logdir runs``.
3. b. If it throws an error, try ``pip uninstall tensorboard-plugin-wit``
4. Open http://localhost:6006/ and switch to the ``Scalars`` tab (right upper corner).