import os

import math
import shutil


def make_subfolders(source_folder, destination_root):
    """
    split the dataset in the source_folder in many subfolders in the destination_root

    source, destination must be different
    subfolder have the size 1000 except the last one

    :param source_folder: source
    :param destination_root: destination
    :return: nothing
    """
    files = os.listdir(source_folder)
    number_files = len(files)
    print(number_files)

    num_subdirectorys = math.ceil(number_files / 1000)
    print(num_subdirectorys)

    print(files[0:10])

    os.mkdir(destination_root)
    for i in range(num_subdirectorys):
        print("Subdir: ", i)
        files = os.listdir(source_folder)
        number_files = len(files)
        subdir_path = destination_root + "paket" + str(i + 1) + "/"
        os.mkdir(subdir_path)
        for f in range(0, min(1000, number_files)):
            shutil.move(source_folder + files[f], subdir_path)


if __name__ == "__main__":
    source = "a"
    des_root = "b"
    make_subfolders(source, des_root)
