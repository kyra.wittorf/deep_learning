i=0

# erstes argument klasse
# zweites argument quelle

if [ "$1" != "" ] &&  [ "$2" != "" ]
then

<<KOMMENTARIO
for f in *.gif
do
	convert -verbose -coalesce $f bild$i.jpg
	rm -f $f
	((i += 1))
done

for f in *.png
do
	convert -verbose -coalesce $f bild$i.jpg
	rm -f $f
	((i += 1))
done

for f in *.jpeg
do
	convert -verbose -coalesce $f bild$i.jpg
	rm -f $f
	((i += 1))
done
KOMMENTARIO

n=100
for i in *.jpg
do
	mv -n -- ./"$i" "$1_$2_$n(2).jpg"
	((n += 1))
done

# ls *.jpg | cat -n | while read n f; do mv -- "$f" "$1_$2_$n(2).jpg"; done

fi