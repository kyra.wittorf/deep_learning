# Daten

|    		| amazon 	| ebay 	| microsoft |
| -----		| ------	| ---- 	| --------- |
| Flickr	| 189		| 98   	| 140 	   	|
| WebLogo2M | 830 		| 782  	| 780	   	|
| OpenLogo	| 70		| 20	| 28		|
| Google 	| 34 		| 40  	| 15	   	|
|			|			|		|			|
| Insgesamt | 932		| 934  	| 963	   	| 


Test und Trainingsdaten setzen sich derzeit aus allen Daten außer von Google zusammen.

Wobei 85\% Trainingsdaten und 15\% Testdaten sind