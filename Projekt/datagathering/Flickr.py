import flickrapi
import urllib.request as request
from datagathering.config import *


def get_images(save_begin: int, save_filename: str, keyword: str, begin: int, end: int):
    """
    get end-begin fotos from flickr, when you search for the keyword
    and save this images in <save_filename><save_begin>.<file_ending_type> (count save_begin up)
    the folder is Data/newData

    url_o: get the original images from flickr (no size change)
    :param save_begin: the first number of the save image
    :param save_filename: the file_name of the save image
    :param keyword: to search for
    :param begin: begin number of the images to crawl
    :param end: end number of the images to crawl
    :return: nothing; but saves images
    """

    flickr = flickrapi.FlickrAPI(key_long, key, cache=True)

    photos = flickr.walk(text=keyword,
                         tag_mode='all',
                         # tags=keyword,
                         extras='url_o',
                         per_page=200,  # may be you can try different numbers..
                         sort='relevance')

    print(photos)
    urls = []
    for i, photo in enumerate(photos):
        print(i)

        url = photo.get('url_o')
        urls.append(url)

        # get end urls
        if i > end:
            break

    print(urls)

    for i in range(begin, urls.__len__()):
        # Download image from the url and save it to for example 'ebay1.jpg'
        if urls[i] is not None:
            file_ending_typ = urls[i][-4:]
            request.urlretrieve(urls[i], '../Data/newData/%s%s%s' % (save_filename, i + save_begin, file_ending_typ))


# Resize the image and overwrite it
# image = Image.open('00001.jpg')
# image = image.resize((256, 256), Image.ANTIALIAS)
# image.save('00001.jpg')


if __name__ == "__main__":
    get_images(save_begin=0, save_filename="ebay", keyword="ebay logo", begin=0, end=300)
